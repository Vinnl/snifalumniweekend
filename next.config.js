const remarkGfm = require("remark-gfm");

const withMDX = require('@next/mdx')({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [remarkGfm],
    rehypePlugins: [],
  },
});
module.exports = withMDX({
  pageExtensions: ['js', 'jsx', 'mdx', 'md', 'ts', 'tsx'],
});
