import { ChangeEventHandler, FormEventHandler, useState } from "react";
import jsSHA from "jssha";
import Jelle from "./answers/jelle.mdx";
import Florian from "./answers/florian.mdx";
import JensEnDiede from "./answers/dens.mdx";
import ThirzaEnFlorian from "./answers/thirzaenflorian.mdx";
import Roos from "./answers/roos.mdx";
import Jelte from "./answers/jelte.mdx";
import Susan from "./answers/susan.mdx";
import Nienke from "./answers/nienke.mdx";
import Joris from "./answers/joris.mdx";
import Matthias from "./answers/matthias.mdx";
import Eline from "./answers/eline.mdx";
import Anke from "./answers/anke.mdx";
import Emma from "./answers/emma.mdx";
import Colin from "./answers/colin.mdx";
import Vincent from "./answers/vincent.mdx";
import Yorick from "./answers/yorick.mdx";
import Usb from "./answers/usb.mdx";

const instructions: Record<string, JSX.Element> = {
  "bbb714e86eb57809f21818412d362e03f7ee220e15f8afd7d853254fcac4156276502b8d5092a56113fe0d095f63339a833c5ab712093d0d5b34c5eca92e2689": 
    <Jelle/>,
  "40755d7a4a208d61f246d7aaaf6bef7b52e31bb228feffd51e26d449f42f7b26e49905828fb31b312e15959719160cdf8b70e0a433e6723f772b17ffff315a48":
    <Florian/>,
  "5ec5e1c8a5774f1b09fe195e573a7aa710cd4c0479ec50fbca146439e0673efc7c5efd2d80ea51554f51cb8a6c98cfe8d72d92eb9d1a300dc0f42d59ea9b700f":
    <JensEnDiede/>,
  "cfd4aeb37e86243d2fb115e715dca3b870c1cd319f6b3f2bf0ea62e711ecd88f3936c6086cfc320659707f92d6e17ead37e5d339d5df9f896c44bd90c06d2b8e":
    <ThirzaEnFlorian/>,
  "62cfb6b0be5a750cb4da4010bc6e3564a68cd317403ab0d8fbacae1992e2dd67363f372681800011c2d877cd86a7a293ae45aa975adc5e9565c663925ad5e7c1":
    <Roos/>,
  "20c0996f32c1a67aae87aa659014aa95309c105938d5a1f2e1fbcd2a1936b86ace165464f99bcb9847e37de8d677c6a1bff6d1b9fd1237bf0986a34122924569":
    <Jelte/>,
  "5797cd653f89661983b918fade261b044b439317238c27cf20d4cb39e5abe2bcb12bb9cafd165b908d6d6861fbfad947623d0fef48bbc7065cfeeb2079e569b5":
    <Susan/>,
  "7e2643b6704c2ee965c4d229092dab68155839942a4beeab146d2e5a94db9896a50ed9bc396c31a05f0b7339ac70028979fc232a873923deb1164295c863953e":
    <Nienke/>,
  "a709794a2bd8404bd915ad2f439c618e691d43e5fd7106662333e023abc083aa6827c1766c95482a428c1db1c7deba75cebb8710113fcc6cf5046f012170be34":
    <Joris/>,
  "1c01d6875eafc6762de148f8181594be7cbd162b8cec669012de4dd8442df581fc156668bda93f96ac345ce5479bd927e5fab26b56973b81c84a8a76b3857a30":
    <Matthias/>,
  "428eaba21a0347bdbc6f6aa5a06d627fbb0b23605d146705397ea02692be15ee4198051e246b627c5a0887aa18482df47135b9062ba2eb1995e85a096c576bf0":
    <Eline/>,
  "a5e48a0e37c460d0730eb81afe39c54ef0d5de6de2959727c7ccd68b0e1f58edfc73b23bda8b8a2858bc396cb81211ec3f4ccab70471dd85a8c6d5c980b6f120":
    <Anke/>,
  "8efe27d29331b6f21b7014085cc73f299cfd088bf4b70353908aac1294e7f66ae80ae4240f6250bda27e08c690ad7980c41f577ea5b69bd411dfd8a626d38770":
    <Emma/>,
  "737e28379558d5d1a0b7d935e1e23b4d99d4813725165212c7b2ea033426c1a1ec063ed743627e8ae6f4b2308a676a6324f419f857e86b61f20f16bde78890ac":
    <Colin/>,
  "39e5a8d09af03f8a4bc8a99e9176ed917fb060f689831e2259b0efde5de67ca9199c27a58716b38678d3bba767b5c30e0ba43f554a0d89b57f3f33c69ca8f25f":
    <Vincent/>,

 "2c1a1ae686a1bdfd15a3c7ee26be64a42a72c2f816884dad2ed2b7d850e4390f1a880e6e2d7694d28ddeec156c79522f086aaea9a471df0f3cfc7d6efad8cafc":<Yorick/>,
"6a0fdaf6c3e95ce89649fc9bd0b2e983d60b161ac90f4b88274d65ec66d7b2fe7faeac62ede04dbd423aae8240c096ad4304ee63fc847940a9b12e991655296b":
  <Usb/>,
    
};

export const PasswordEntry = () => {
  const [password, setPassword] = useState("");

  const hashObject = new jsSHA("SHA-512", "TEXT", { encoding: "UTF8" });
  hashObject.update(password);
  const hashedPassword = hashObject.getHash("HEX");
  const instructionElement = instructions[hashedPassword] ?? null;

  const updatePassword: ChangeEventHandler<HTMLInputElement> = (e) => {
    e.preventDefault();

    setPassword(e.target.value);
  }

  const onSubmit: FormEventHandler = (e) => {
    e.preventDefault();
  };

  const formClass = instructionElement === null
    ? ""
    : "text-gray-400";
  const instructionClass = instructionElement === null
    ? ""
    : "mt-9 pt-9";

  return (
    <>
      <form onSubmit={onSubmit} className={formClass}>
        <label
          htmlFor="password"
          className="pr-2"
        >Wachtwoord:</label>
        <input
          type="text"
          id="password"
          value={password}
          onChange={updatePassword}
          className="border-gray border-b-2"
        />
      </form>
      <div className={instructionClass}>
        {instructionElement}
      </div>
    </>
  );
}
