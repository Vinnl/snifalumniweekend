import Head from 'next/head';
import '../../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Snif… Alumniweekend</title>
      </Head>
      <article className="prose lg:prose-xl mx-auto p-5 pt-10">
        <Component {...pageProps} />
      </article>
    </>
  );
}

export default MyApp
